# Progress Bar Demo

## Output

We need a [progress bar like this](https://getbootstrap.com/docs/4.1/components/progress/)
which shows percentage of purchased products total cost (purchasedProducts) vs total budget you have (totalBudget).
Progress bar should have a label explaining progress i.e: "You already have spend 35% of your total budget money".
* It should be reusable component.
* Please do not use bootstrap version of progress bar and create all css on your own.
* You can install third party libraries from npm if you need it. 
* You can use google at any time to look for documentation etc.

## Input

You can use Apiary to query products: [Products API](swointerview.docs.apiary.io) or mock data source pretending
to be API call which returns a list of already purchased products and total budget you have for spending.

purchased Products endpoint [GET](https://private-ea9a9-swointerview.apiary-mock.com/purchased-products)

```json
{
  "purchasedProducts": [
    {
      "name": "Product 1",
      "cost": 100
    },
    {
      "name": "Product 2",
      "cost": 125
    },
    {
      "name": "Product 3",
      "cost": 200
    }
  ],
  "totalBudget": 1000 
}
```

## Extra

Add animation, change data dynamically.
